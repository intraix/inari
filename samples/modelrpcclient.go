package main

import (
	"encoding/json"
	"fmt"
	"log"
	"math/rand"
	"os"

	"bitbucket.org/intraix/inari/weather"
	"github.com/streadway/amqp"
)

func failOnError(err error, msg string) {
	if err != nil {
		fmt.Printf("%s: %s", msg, err)
		panic(fmt.Sprintf("%s: %s", msg, err))
	}
}

func randomString(l int) string {
	bytes := make([]byte, l)
	for i := 0; i < l; i++ {
		bytes[i] = byte(randInt(65, 90))
	}
	return string(bytes)
}

func randInt(min int, max int) int {
	return min + rand.Intn(max-min)
}

func main() {

	//Create rabbitmq connection and create channel for queues
	conn, err := amqp.Dial("amqp://guest:running201201026W@localhost:5672/")
	failOnError(err, "Failed to connect to RabbitMQ")
	defer conn.Close()

	ch, err := conn.Channel()
	failOnError(err, "Failed to open a channel")

	defer ch.Close()

	//Create callback queue. Mark it exclusive so it gets deleted after client closes connection.
	callback, err := ch.QueueDeclare(
		"",    // name
		false, // durable
		false, // delete when usused
		true,  // exclusive
		false, // noWait
		nil,   // arguments
	)
	failOnError(err, "Failed to declare a queue")

	// Create correlation ID to tag the right output for your input. Usually it is a generated UUID.
	corrId := randomString(32)

	// Sample json query  to be sent
	body := []byte(`{"hub_identifier": "1"}`)

	// Publish to rpc_queue
	err = ch.Publish(
		"",                  // exchange
		"weather_rpc_queue", // routing key
		false,               // mandatory
		false,               // immediate
		amqp.Publishing{
			Headers:         amqp.Table{},
			ContentType:     "application/json",
			ContentEncoding: "",
			Body:            body,
			DeliveryMode:    amqp.Transient, // 1=non-persistent, 2=persistent
			Priority:        0,              // 0-9,
			ReplyTo:         callback.Name,
			CorrelationId:   corrId,
		})
	failOnError(err, "Failed to publish a message")

	// Wait to consume results on the callback queue that has been created
	msgs, err := ch.Consume(callback.Name, "", true, false, false, false, nil)
	failOnError(err, "Failed to register a consumer")

	done := make(chan bool)
	var d amqp.Delivery
	var returnValue weather.WeatherReturn

	go func() {
		for d = range msgs {
			// Check correlation ID and parse results byte array into json objects.
			if d.CorrelationId == corrId {
				json.Unmarshal(d.Body, &returnValue)
				done <- true
			}
			if returnValue.Status == 400 {
				log.Println("Error:", returnValue.Error)
			} else {
				log.Println("Received:", returnValue.Payload)
			}
		}
	}()

	log.Printf("[*] Waiting for reply. To exit press CTRL+C")
	select {
	case <-done:
		break
	}
	log.Printf("Done")

	os.Exit(0)
}
