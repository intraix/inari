package db

import (
	"database/sql"

	_ "github.com/lib/pq"

	"log"
)

type Postgres struct {
	Session *sql.DB
}

var Pg Postgres

func (p *Postgres) Init(dbPath string) (err error) {
	p.Session, err = sql.Open("postgres", dbPath)
	return
}

func (p *Postgres) Close() {
	log.Println("Closing postgres connection")
	p.Session.Close()
}
