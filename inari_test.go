package main

import (
	"encoding/json"
	"log"
	"os"
	"testing"
	"time"

	"bitbucket.org/intraix/inari/db"
	"bitbucket.org/intraix/inari/janitor"
	"bitbucket.org/intraix/inari/weather"
)

func TestMain(m *testing.M) {
	initPg() //Setup
	retCode := m.Run()
	closePg() //Teaardown
	os.Exit(retCode)
}

func initPg() {
	pgDB = "postgres://postgres@localhost/vanern_test?sslmode=disable"
	err := db.Pg.Init(pgDB)
	if err != nil {
		log.Println("PGERR Cannot open postgres session")
		panic(err)
	}
	_, _ = db.Pg.Session.Query(`INSERT INTO locations  values (1,'test')`)
	_, _ = db.Pg.Session.Query(`INSERT INTO addresses (id, addressable_id, lat, lng) values (1, 1, 1.352083, 103.819836)`)
	_, _ = db.Pg.Session.Query(`INSERT INTO hubs values (1,1,'1')`)
}

func closePg() {
	_, _ = db.Pg.Session.Query(`TRUNCATE locations`)
	_, _ = db.Pg.Session.Query(`TRUNCATE addresses`)
	_, _ = db.Pg.Session.Query(`TRUNCATE hubs`)

	db.Pg.Close()
}

func TestEndtoEndSuccess(t *testing.T) {
	var hub_identifier = "1"

	var w weather.WeatherLoader
	var openWeather weather.OpenWeatherParams
	w = openWeather

	inariMap = janitor.NewBroom(w)
	weatherByte, err := inariMap.GetData(hub_identifier, 15*time.Minute)

	if err != nil {
		t.Error(err)
	}

	var testData weather.WeatherParams
	err = json.Unmarshal(weatherByte, &testData)
	if err != nil {
		t.Error("JSON_MARSHALL_ERR", err)
	}
}

func TestEndtoEndFail(t *testing.T) {
	var hub_identifier = "123"

	var w weather.WeatherLoader
	var openWeather weather.OpenWeatherParams
	w = openWeather

	inariMap = janitor.NewBroom(w)
	weatherByte, err := inariMap.GetData(hub_identifier, 15*time.Minute)

	if err == nil {
		t.Error("Expected error but none returned")
	}

	if len(weatherByte) != 0 {
		t.Error("Expected no return data but returned []byte", weatherByte)
	}
}
