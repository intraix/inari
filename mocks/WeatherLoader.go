package mocks

import "github.com/stretchr/testify/mock"

type WeatherLoader struct {
	mock.Mock
}

func (m *WeatherLoader) LoadData(hub_identifier string) ([]byte, error) {
	ret := m.Called(hub_identifier)

	var r0 []byte
	if ret.Get(0) != nil {
		r0 = ret.Get(0).([]byte)
	}
	r1 := ret.Error(1)

	return r0, r1
}
