package main

import (
	"encoding/json"
	"log"
	"log/syslog"
	"os"
	"os/signal"
	"strings"
	"syscall"
	"time"

	"bitbucket.org/intraix/inari/db"
	"bitbucket.org/intraix/inari/janitor"
	"bitbucket.org/intraix/inari/weather"

	"github.com/kelseyhightower/envconfig"
	"github.com/streadway/amqp"
)

type Specification struct {
	Dev             string
	Db              string
	Rabbit_username string
	Rabbit_password string
	Pg_user         string
}

var (
	config   Specification
	pgDB     string //Postgres DB
	uri      string //AMQP URI
	queue    string //Ephemeral AMQP queue name
	inariMap *janitor.Broom
)

type Params struct {
	HubIdentifier string `json:"hub_identifier"`
}

type LocationCoords struct {
	latitude  float32
	longitude float32
}

type Consumer struct {
	conn    *amqp.Connection
	channel *amqp.Channel
	tag     string
	done    chan error
}

func init() {
	err := envconfig.Process("inari", &config)
	if err != nil {
		log.Fatal(err)
	}
	if config.Dev != "development" {
		// Configure logger to write to the syslog
		syslogw, e := syslog.New(syslog.LOG_NOTICE, "inari")
		if e == nil {
			log.SetOutput(syslogw)
		}
	}
	pgDB = "postgres://" + config.Pg_user + "@localhost/" + config.Db + "?sslmode=disable"
	uri = "amqp://" + config.Rabbit_username + ":" + config.Rabbit_password + "@localhost:5672/"
	queue = "weather_rpc_queue"

	var w weather.WeatherLoader
	var openWeather weather.OpenWeatherParams
	w = openWeather

	inariMap = janitor.NewBroom(w)
}

func main() {
	// Set up channel on which to send signal notifications.
	sigc := make(chan os.Signal, 1)
	signal.Notify(sigc, syscall.SIGINT, syscall.SIGKILL, syscall.SIGTERM)

	log.Printf("Started with config: %+v", config)

	err := db.Pg.Init(pgDB)
	if err != nil {
		log.Println("PGERR Cannot open postgres session")
		panic(err)
	}

	c, err := NewConsumer(uri, queue)
	if err != nil {
		log.Fatalf("%s", err)
	}

	defer db.Pg.Close()

	// Wait for receiving a signal. This will clean exit on SIGINT, SIGKILL and SIGTERM and ensures the defers run.
	<-sigc

	log.Printf("shutting down")

	if err := c.Shutdown(); err != nil {
		log.Fatalf("error during shutdown: %s", err)
	}
}

// Central request processing unit
func processRequest(value []byte, c *Consumer, d amqp.Delivery) {
	var parameters Params

	err := json.Unmarshal(value, &parameters)
	if err != nil {
		handleErrors(c, d, "ERR json unmarshal: "+err.Error())
		return
	}

	log.Println("Received:", parameters.HubIdentifier)

	hub_identifier := strings.ToUpper(parameters.HubIdentifier)

	weatherData, err := inariMap.GetData(hub_identifier, 15*time.Minute)
	if err != nil {
		handleErrors(c, d, err.Error())
		return
	}

	sendData(c, d, weatherData)
}

// Send data
func sendData(c *Consumer, d amqp.Delivery, weatherData []byte) {
	var returnParams weather.WeatherReturn
	returnParams.Status = 200
	err := json.Unmarshal(weatherData, &returnParams.Payload)
	if err != nil {
		handleErrors(c, d, "JSON_UNMARSHAL_ERROR "+err.Error())
		return
	}

	payload, _ := json.Marshal(&returnParams)

	log.Println("Returned", returnParams.Status)
	replyQueue(c, d, payload)
}

// Handle errors
func handleErrors(c *Consumer, d amqp.Delivery, msg string) {
	var returnParams weather.WeatherReturn
	returnParams.Status = 400
	returnParams.Error = msg
	payload, _ := json.Marshal(&returnParams)
	log.Println("Returned", returnParams.Status)
	replyQueue(c, d, payload)
}

// Sends out the result throught the callback with the respective callback ID
func replyQueue(c *Consumer, d amqp.Delivery, b []byte) {
	err := c.channel.Publish(
		"",        // exchange
		d.ReplyTo, // routing key
		false,     // mandatory
		false,     // immediate
		amqp.Publishing{
			ContentType:   "application/json",
			Body:          b,
			CorrelationId: d.CorrelationId,
		})
	if err != nil {
		log.Println("ERR Failed to publish result", err)
	}
}

//Creates a new rabbitmq connection, to receive from RPC queue
func NewConsumer(amqpURI, queueName string) (*Consumer, error) {
	c := &Consumer{
		conn:    nil,
		channel: nil,
		tag:     "",
		done:    make(chan error),
	}

	var err error

	log.Printf("dialing %q", amqpURI)
	c.conn, err = amqp.Dial(amqpURI)
	if err != nil {
		log.Printf("ERR Dial: %s", err)
	}

	go func() {
		log.Println("closing errors:", <-c.conn.NotifyClose(make(chan *amqp.Error)))
	}()

	log.Printf("got Connection, getting Channel")
	c.channel, err = c.conn.Channel()
	if err != nil {
		log.Printf("ERR Channel: %s", err)
	}

	log.Printf("declaring Queue %q", queueName)
	queue, err := c.channel.QueueDeclare(
		queueName, // name of the queue
		false,     // durable
		false,     // delete when usused
		false,     // exclusive
		false,     // noWait
		nil,       // arguments
	)
	if err != nil {
		log.Printf("ERR Queue Declare: %s", err)
	}

	log.Printf("declared Queue (%q %d messages, %d consumers",
		queue.Name, queue.Messages, queue.Consumers)

	deliveries, err := c.channel.Consume(
		queue.Name, // name
		c.tag,      // consumerTag,
		false,      // noAck
		false,      // exclusive
		false,      // noLocal
		false,      // noWait
		nil,        // arguments
	)
	if err != nil {
		log.Printf("ERR Queue Consume: %s", err)
	}

	go c.handle(deliveries, c.done)

	return c, nil
}

// Rabbitmq shutdown handling
func (c *Consumer) Shutdown() error {
	// will close() the deliveries channel
	if err := c.channel.Cancel(c.tag, true); err != nil {
		log.Printf("ERR Consumer cancel failed: %s", err)
	}

	if err := c.conn.Close(); err != nil {
		log.Printf("ERR AMQP connection close error: %s", err)
	}

	defer log.Printf("AMQP shutdown OK")

	// wait for handle() to exit
	return <-c.done
}

//Handle deliveries coming in through 'rpc_queue'
func (c *Consumer) handle(deliveries <-chan amqp.Delivery, done chan error) {
	for d := range deliveries {
		go processRequest(d.Body, c, d)
		d.Ack(true)
	}
	log.Printf("handle: deliveries channel closed")
	done <- nil
}
