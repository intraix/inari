package weather

import (
	"errors"
	"io/ioutil"
	"log"
	"net/http"
	"strconv"

	"bitbucket.org/intraix/inari/db"
)

type WeatherReturn struct {
	Status  int           `json:"status"`
	Payload WeatherParams `json:"payload"`
	Error   string        `json:"error"`
}

type WeatherParams struct {
	Coord struct {
		Lon float32 `json:"lon"`
		Lat float32 `json:"lat"`
	} `json:"coord"`
	Sys struct {
		Type    int     `json:"type"`
		ID      int     `json:"id"`
		Message float32 `json:"message"`
		Country string  `json:"country"`
		Sunrise int64   `json:"sunrise"`
		Sunset  int64   `json:"sunset"`
	} `json:"sys"`
	Weather []struct {
		ID          int    `json:"id"`
		Main        string `json:"main"`
		Description string `json:"description"`
		Icon        string `json:"icon"`
	} `json:"weather"`
	Base string `json:"base"`
	Main struct {
		Temp     float32 `json:"temp"`
		Pressure float32 `json:"pressure"`
		Humidity float32 `json:"humidity"`
		TempMin  float32 `json:"temp_min"`
		TempMax  float32 `json:"temp_max"`
	} `json:"main"`
	Wind struct {
		Speed  float32 `json:"speed"`
		Deg    float32 `json:"deg"`
		VarBeg float32 `json:"var_beg"`
		VarEnd float32 `json:"var_end"`
	} `json:"wind"`
	Clouds struct {
		All int `json:"all"`
	} `json:"clouds"`
	Dt   int64  `json:"dt"`
	ID   int64  `json:"id"`
	Name string `json:"name"`
	Cod  int    `json:"cod"`
}

type OpenWeatherParams struct {
	Coord struct {
		Lon float32 `json:"lon"`
		Lat float32 `json:"lat"`
	} `json:"coord"`
	Sys struct {
		Type    int     `json:"type"`
		ID      int     `json:"id"`
		Message float32 `json:"message"`
		Country string  `json:"country"`
		Sunrise int64   `json:"sunrise"`
		Sunset  int64   `json:"sunset"`
	} `json:"sys"`
	Weather []struct {
		ID          int    `json:"id"`
		Main        string `json:"main"`
		Description string `json:"description"`
		Icon        string `json:"icon"`
	} `json:"weather"`
	Base string `json:"base"`
	Main struct {
		Temp     float32 `json:"temp"`
		Pressure float32 `json:"pressure"`
		Humidity float32 `json:"humidity"`
		TempMin  float32 `json:"temp_min"`
		TempMax  float32 `json:"temp_max"`
	} `json:"main"`
	Wind struct {
		Speed  float32 `json:"speed"`
		Deg    float32 `json:"deg"`
		VarBeg float32 `json:"var_beg"`
		VarEnd float32 `json:"var_end"`
	} `json:"wind"`
	Clouds struct {
		All int `json:"all"`
	} `json:"clouds"`
	Dt   int64  `json:"dt"`
	ID   int64  `json:"id"`
	Name string `json:"name"`
	Cod  int    `json:"cod"`
}

const openWeatherKey = "f80e91fe247278964774dde22f05c8fa"

type WeatherLoader interface {
	LoadData(hub_identifier string) (weatherByte []byte, err error)
}

func (weatherData OpenWeatherParams) LoadData(hub_identifier string) (weatherByte []byte, err error) {
	var lat, lng float32
	rows, err := db.Pg.Session.Query(`SELECT lat,lng from addresses a, hubs h, locations l where h.hub_identifier = $1 and l.id = h.location_id and a.addressable_id=l.id`, hub_identifier)
	if err != nil {
		panic(err)
	}

	defer rows.Close()

	for rows.Next() {
		err = rows.Scan(&lat, &lng)
		if err != nil {
			return make([]byte, 0), errors.New("QUERY_ERR " + err.Error())
		}
	}

	if lat == 0 || lng == 0 {
		return make([]byte, 0), errors.New("Address coordinates code not valid")
	}

	log.Println("Address Coordinates:", lat, lng)
	weatherQueryString := "http://api.openweathermap.org/data/2.5/weather?lat=" + strconv.FormatFloat(float64(lat), 'f', 4, 32) + "&lon=" + strconv.FormatFloat(float64(lng), 'f', 4, 32) + "&APPID=" + openWeatherKey
	response, err := http.Get(weatherQueryString)
	defer response.Body.Close()

	log.Println("Getting status from:", weatherQueryString)

	if err != nil {
		return make([]byte, 0), errors.New("OPEN_WEATHER_ERR " + err.Error())
	} else {
		weatherByte, err = ioutil.ReadAll(response.Body)
		if err != nil {
			return make([]byte, 0), errors.New("OPEN_WEATHER_ERR " + err.Error())
		}
	}

	return
}
