package janitor

import (
	"log"
	"sync"
	"time"

	"bitbucket.org/intraix/inari/weather"
)

type latestWeather struct {
	timestamp time.Time
	data      []byte
}

type Broom struct {
	janitorMap map[string]latestWeather
	loader     weather.WeatherLoader
	sync.RWMutex
}

func NewBroom(w weather.WeatherLoader) *Broom {
	var b Broom
	b.janitorMap = make(map[string]latestWeather)
	b.loader = w
	return &b
}

func (j *Broom) GetData(hub_identifier string, expireAfter time.Duration) (weatherData []byte, err error) {
	j.RLock()
	if _, present := j.janitorMap[hub_identifier]; present {
		if time.Now().Add(-expireAfter).Before(j.janitorMap[hub_identifier].timestamp) {
			log.Println("Taking from cache for hub_identifier:", hub_identifier)
			weatherData = j.janitorMap[hub_identifier].data
			j.RUnlock()
		} else {
			log.Println("Data old in cache. Loading new data from Open weather for hub_identifier:", hub_identifier)
			j.RUnlock()
			weatherData, err = j.loadOpenWeatherData(hub_identifier)
		}
	} else {
		log.Println("No data in cache. Loading new data from Open weather for hub_identifier:", hub_identifier)
		j.RUnlock()
		weatherData, err = j.loadOpenWeatherData(hub_identifier)
	}
	return
}

func (j *Broom) loadOpenWeatherData(hub_identifier string) (weatherData []byte, err error) {
	weatherData, err = j.loader.LoadData(hub_identifier)
	if len(weatherData) != 0 {
		j.Lock()
		j.janitorMap[hub_identifier] = latestWeather{time.Now(), weatherData}
		j.Unlock()
	}
	return
}
