package janitor

import (
	"encoding/json"
	"testing"
	"time"

	"bitbucket.org/intraix/inari/mocks"
	"bitbucket.org/intraix/inari/weather"
)

func TestNewLoad(t *testing.T) {
	hub_identifier := "1"
	w := new(mocks.WeatherLoader)
	mockData := weather.OpenWeatherParams{
		Main: struct {
			Temp     float32 `json:"temp"`
			Pressure float32 `json:"pressure"`
			Humidity float32 `json:"humidity"`
			TempMin  float32 `json:"temp_min"`
			TempMax  float32 `json:"temp_max"`
		}{
			Temp:     23.5,
			Pressure: 54,
			Humidity: 46,
			TempMin:  21,
			TempMax:  27,
		},
	}
	mockByteArray, _ := json.Marshal(&mockData)
	w.On("LoadData", hub_identifier).Return(mockByteArray, nil)

	j := NewBroom(w)
	_, err := j.GetData("1", 2*time.Second)

	if err != nil {
		t.Error(err)
	}

	if len(j.janitorMap) != 1 {
		t.Error("Expected length: 1, Got length:", len(j.janitorMap))
	}

	w.AssertExpectations(t)
}

func TestOldLoad(t *testing.T) {
	hub_identifier := "1"
	w := new(mocks.WeatherLoader)
	mockData := weather.OpenWeatherParams{
		Main: struct {
			Temp     float32 `json:"temp"`
			Pressure float32 `json:"pressure"`
			Humidity float32 `json:"humidity"`
			TempMin  float32 `json:"temp_min"`
			TempMax  float32 `json:"temp_max"`
		}{
			Temp:     23.5,
			Pressure: 54,
			Humidity: 46,
			TempMin:  21,
			TempMax:  27,
		},
	}
	mockByteArray, _ := json.Marshal(&mockData)
	w.On("LoadData", hub_identifier).Return(mockByteArray, nil)

	j := NewBroom(w)
	_, err := j.GetData("1", 2*time.Second)

	_, err = j.GetData("1", 2*time.Second)

	if err != nil {
		t.Error(err)
	}

	if len(j.janitorMap) != 1 {
		t.Error("Expected length: 1, Got length:", len(j.janitorMap))
	}

	w.AssertExpectations(t)
	w.AssertNumberOfCalls(t, "LoadData", 1)
}

func TestRefreshLoad(t *testing.T) {
	hub_identifier := "1"
	w := new(mocks.WeatherLoader)
	mockData := weather.OpenWeatherParams{
		Main: struct {
			Temp     float32 `json:"temp"`
			Pressure float32 `json:"pressure"`
			Humidity float32 `json:"humidity"`
			TempMin  float32 `json:"temp_min"`
			TempMax  float32 `json:"temp_max"`
		}{
			Temp:     23.5,
			Pressure: 54,
			Humidity: 46,
			TempMin:  21,
			TempMax:  27,
		},
	}
	mockByteArray, _ := json.Marshal(&mockData)
	w.On("LoadData", hub_identifier).Return(mockByteArray, nil)

	j := NewBroom(w)
	_, err := j.GetData("1", 100*time.Millisecond)

	time.Sleep(300 * time.Millisecond)

	_, err = j.GetData("1", 100*time.Millisecond)

	if err != nil {
		t.Error(err)
	}

	if len(j.janitorMap) != 1 {
		t.Error("Expected length: 1, Got length:", len(j.janitorMap))
	}

	w.AssertExpectations(t)
	w.AssertNumberOfCalls(t, "LoadData", 2)
}

func TestNewDataLoad(t *testing.T) {
	w := new(mocks.WeatherLoader)
	mockData := weather.OpenWeatherParams{
		Main: struct {
			Temp     float32 `json:"temp"`
			Pressure float32 `json:"pressure"`
			Humidity float32 `json:"humidity"`
			TempMin  float32 `json:"temp_min"`
			TempMax  float32 `json:"temp_max"`
		}{
			Temp:     23.5,
			Pressure: 54,
			Humidity: 46,
			TempMin:  21,
			TempMax:  27,
		},
	}
	mockByteArray, _ := json.Marshal(&mockData)
	w.On("LoadData", "1").Return(mockByteArray, nil)
	w.On("LoadData", "2").Return(mockByteArray, nil)

	j := NewBroom(w)
	_, err := j.GetData("1", 1*time.Second)

	_, err = j.GetData("2", 1*time.Second)

	if err != nil {
		t.Error(err)
	}

	if len(j.janitorMap) != 2 {
		t.Error("Expected length: 1, Got length:", len(j.janitorMap))
	}

	w.AssertExpectations(t)
	w.AssertNumberOfCalls(t, "LoadData", 2)
}
